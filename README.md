![Banner](https://divestos.org/images/featureGraphics/Mulch.png)

Mulch
=====
The DivestOS WebView

Use
---
- This repository contains the compile scripts, patches, and prebuilt WebView providers
- The WebView here is not meant to be used as is, but compiled into an OS
- Standalone versions of Mulch for all systems are available here: https://divestos.org/index.php?page=our_apps#mulch

Goals
-----
- Minimizing anti-features
- Included patches must be very simple to minimize maintenance
- Becoming a "privacy" browser is out of scope, use Mull instead

Notes
-----
- This repo will only keep one version at a time
- This repo will be reinitialized often
- x86 and x86_64 will track from LineageOS due to excessive compile times

Credits
-------
- Nearly all of the patches are from GrapheneOS's Vanadium browser
    - License: GPL-2.0-only with exceptions
    - Upstream: https://github.com/GrapheneOS/Vanadium
- The build script and makefiles are from LineageOS
    - License: Apache-2.0
- (previously) Additional patches from Bromite
    - License: GPL-3.0
    - Upstream: https://github.com/bromite/bromite
- Banner backdrop from Paul Green
    - License: Unsplash
    - Author: https://unsplash.com/@pgreen1983
    - Original: https://unsplash.com/photos/mGQfQe3EOBI
